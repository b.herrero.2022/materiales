# Ejercicios

## Introducción

### <a name="segundos">Días, horas, minutos y segundos</a>

Escribe un programa en Python3 que lea (de teclado) una cantidad de segundos y los convierta en días, horas, minutos y segundos. El programa, al arrancar, mostrará un mensaje en en pantalla:

```
Número de segundos:
```

y esperará que el usuario escriba un número entero, y la tecla ·"Enter". A continuación escribirá:

```
xxx segundos son ddd día(s), hhh hora(s), mmm minuto(s) y sss segundo(s)
```

Por ejemplo, si se introduce el número 90061, el programa escribirá:

```
90061 segundos son 1 día(s), 1 hora(s), 1 minuto(s) y 1 segundo(s)
```

En la cabecera del programa, pon un comentario que indique qué hace el programa, y el nombre de quien lo ha escrito.

* [Solución](intro-segundos/segundos.py)

* [Solución con funciones y tests](intro-segundos-2/segundos.py) (para probar tests)

* [Solución con funciones y tipos](intro-segundos-3/segundos.py) (para probar MyPy)

## Estructuras de control

### <a name="impares">Suma de números impares</a>

Construir un programa que realice la suma de todos los números impares comprendidos entre dos números enteros no negativos introducidos por teclado por el usuario.

Al arrancar, el programa escribirá: `Dame un número entero no negativo: `. Cuando el usuario escriba el número, el programa escribirá: `Dame otro: `. A continuación, el programa mostrará por pantalla la suma de los números impares comprendidos entre esos dos números, incluidos cualquiera de ellos si es un número impar.

Llama al programa `impares.py`.

* **Fecha de entrega:** 12 de octubre de 2022, 23:59
* Entrega en foro de ejercicios
* [Solución](control-impares/impares.py)

### <a name="primos">Cálculo de números primos</a>

Construir un programa que solicite un número entero no negativo al usuario, escribiendo el mensaje "Dame un número entero no negativo: ", y que como respuesta escriba en una línea el mensaje "Números primos iguales o menores: ", y a continuaciòn, en la misma línea, la lista de los números primos menores o iguales que él, separados por espacios. Para calcular los números primos, utiliza la definición: "un número entero es primo si sólo es divisible por 1 y por él mismo".

Un ejemplo de ejecución podría ser:

```commandline
Dame un número entero no negativo: 11
Números primos iguales o menores: 1 2 3 5 7 11
```

Otro podría ser:

```commandline
Dame un número entero no negativo: 12
Números primos iguales o menores: 1 2 3 5 7 11
```

Llama al programa `primos.py`

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

* **Fecha de entrega:** 19 de octubre de 2022, 23:59

* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/primos
