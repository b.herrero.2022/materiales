# El entorno de programación I

* Frikiminutos Python: [Servidor web con una línea de Python](../../python-snippets/README.md).

## El entorno Linux del laboratorio

Actividades:

* Presentación: los laboratorios Linux de la Escuela.
* Creación de cuentas para los laboratorios Linux (uso de móvil o cuenta alumno / etsit).
* Prueba de cuenta en uno de los ordenadores.
* Presentación: Linux, Ubuntu, GNOME.
* Presentación del escritorio Ubuntu:
  * Entorno de ventanas, lanzamiento de aplicaciones, entrada y salida.
  * Seguimiento de la pantalla compartida del profesor.
  * Entornos de trabajos (workspaces).
  * El gestor gráfico de ficheros
  * El sistema de ficheros:
    * Parte compartida (ejemplo: `/home`)
    * Parte de la máquina (ejemplos: `/tmp`, `/usr`)

Referencias:

* [Sitio web de los laboratorios docentes de la Escuela](https://labs.etsit.urjc.es/)
* [Ubuntu Desktop Guide: Your desktop](https://help.ubuntu.com/stable/ubuntu-help/shell-overview.html.en)
* [Ubuntu Desktop Guide](https://help.ubuntu.com/stable/ubuntu-help/)

## Introducción a la shell (intérprete de comandos)

Actividades:

* Lanzamiento de una consola con intérprete de comandos
* Presentación: el intérprete de comandos de Linux (shell):
  * el prompt
  * `pwd`
  * ejecución de comandos (`fortune`, `date` como ejemplo)
  * flechas (adelante, atrás)
  * `ls`, `ls -l`, `ls -a`, `ls -al`
  * tabulador (completar)
  * `cd` (directorios, `..`, `.`, `~`)
  * `mkdir`, `rmdir`
  * `rm`
  * `du -s ~`
  * `man ls`
* Pestañas de la consola
* Interrumpir un comando: `<CTRL> c`. Ejemplo:
```
ls -R /
<CTRL> c
```
* Copiar y pegar en la consola: `<CTRL> <SHIFT> c`, `<CTRL> <SHIFT>v`
* Lanzamiento del intérprete de Python, y salida
* Python como calculadora

**Ejercicio:** "Ficheros y directorios con el intérprete de comandos" (enunciado en el foro de ejercicios):

* Cámbiate a tu directorio hogar (principal), crea un directorio con `mkdir`, cámbiate a él.
* Crea un fichero con el editor básico de Ubuntu, en ese directorio que acabas de crear, con el contenido "Hola".
* Ejecuta los comandos `pwd`, `ls`, `ls -l` y `ls -al`.
* Contesta al ejercicio en el foro de ejercicios con el resultado de ejecutar los comandos anteriores.
* Para contestar en el foro de ejercicios, copia y pega desde el intérprete de comandos el resultado de ejecutar esos comandos. Recuerda que en el intérprete de comandos puedes copiar un contenido seleccionándolo y pulsando "Mayúsclas"-"Control"-C (y no "Control"-C como se hace en otras aplicaciones). Luego podrás pegar lo copiado en la respuesta, en este foro, con "Control"-V. También puedes usar tanto en el intérprete de comandos como aquí en el foro la opción de, una vez seleccionado, pulsar el botón derecho y usar la opción de menú "Copiar" y luego "Pegar".

Referencias:

* [The Linux command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
* [Linux Command Line Full course](https://www.youtube.com/watch?v=2PGnYjbYuUo) (video)

## El IDE (Integrated Development Environment): PyCharm

Actividades:

* Lanzamiento de PyCharm
* Apertura de un proyecto
* El terminal de PyCharm
* El intérprete de Python de PyCharm. Python como calculadora (2)
* Primer script de Python
* Ejecución

**Ejercicio:** "Operaciones matemáticas simples" (enunciado en el foro de ejercicios).

* Crea en PyCharm un fichero, `operaciones.py`
* Escribe código para escribir, primero, el resultado de sumar 2 y 2, luego, el resultado de multiplicar 3 por 4 (usando la función `print()`.
* El programa deberá mostrar en una línea el resultado de la suma, y en la línea siguiente, el resultado de la multiplicación.
* Ejecútalo, y usa el depurador con él
* Contesta al ejercicio en el foro de ejercicios, incluyendo como adjunto el fichero `operaciones.py` que has creado, cuando funcione bien.
* Ejemplo de solución: [operations.py](operations.py)

Referencias:

* [PyCharm: Get Started](https://www.jetbrains.com/help/pycharm/quick-start-guide.html#code-assistance)
* [PyCharm: First Steps](https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html)
